# Budget

[![pipeline status](https://gitlab.com/terenceponce/budget/badges/master/pipeline.svg)](https://gitlab.com/terenceponce/budget/commits/master)

[![coverage report](https://gitlab.com/terenceponce/budget/badges/master/coverage.svg)](https://gitlab.com/terenceponce/budget/commits/master)

A budgeting app

## Developer Notes

```
$ git clone git@gitlab.com:terenceponce/budget.git
$ cd budget
$ bundle
$ yarn install
$ cp config/database.yml.sample config/database.yml
$ rails db:create db:migrate db:seed
```
