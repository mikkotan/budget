# frozen_string_literal: true

class PagesControllerTest < ActionDispatch::IntegrationTest
  def test_home_success
    get root_url
    assert_response :success
  end
end
