# frozen_string_literal: true

require 'test_helper'

class ListAccountsQueryTest < ActiveSupport::TestCase
  def test_list_accounts_success
    accounts = Queries::ListAccounts.new.call(nil, nil, nil)

    refute_nil accounts
  end
end
