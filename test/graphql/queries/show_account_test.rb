# frozen_string_literal: true

require 'test_helper'

class ShowAccountQueryTest < ActiveSupport::TestCase
  def perform(args = {})
    Queries::ShowAccount.new.call(nil, args, nil)
  end

  def test_show_account_success
    account = perform(id: 2)

    refute_nil account
  end
end
