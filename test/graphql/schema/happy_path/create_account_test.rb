# frozen_string_literal: true

require 'test_helper'

class CreateAccountHappyPathTest < ActiveSupport::TestCase
  def setup
    query_string = %|mutation {
  createAccount(
input: {name: "Wallet", description: "Petty Cash", type: "Cash"}) {
  name description status type }
}|
    context = {}
    variables = {}
    @result = BudgetSchema.execute(query_string,
                                   context: context,
                                   variables: variables)
  end

  def test_create_account_query_returns_correct_name
    assert_equal 'Wallet', @result['data']['createAccount']['name']
  end

  def test_create_account_query_returns_correct_description
    assert_equal 'Petty Cash', @result['data']['createAccount']['description']
  end

  def test_create_account_query_returns_correct_status
    assert_equal 'active', @result['data']['createAccount']['status']
  end

  def test_create_account_query_returns_correct_type
    assert_equal 'Cash', @result['data']['createAccount']['type']
  end
end
