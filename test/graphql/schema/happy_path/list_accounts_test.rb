# frozen_string_literal: true

require 'test_helper'

class ListAccountsHappyPathTest < ActiveSupport::TestCase
  def setup
    query_string = %({ accounts { id name description status type } })
    context = {}
    variables = {}
    @result = BudgetSchema.execute(query_string,
                                   context: context,
                                   variables: variables)
  end

  def test_list_accounts_query_returns_all_accounts
    assert_equal 5, @result['data']['accounts'].count
  end
end
