# frozen_string_literal: true

require 'test_helper'

class CreateUserHappyPathTest < ActiveSupport::TestCase
  def setup
    query_string = %|mutation {
  createUser(authentication: {email: "user@sample.com", password: "secure"}) {
    id
    email
  }}|
    context = {}
    variables = {}
    @result = BudgetSchema.execute(query_string,
                                   context: context,
                                   variables: variables)
  end

  def test_create_user_query_returns_correct_email
    assert_equal 'user@sample.com', @result['data']['createUser']['email']
  end
end
