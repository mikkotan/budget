# frozen_string_literal: true

require 'test_helper'

class ShowAccountHappyPathTest < ActiveSupport::TestCase
  def setup
    query_string = %|{ account(id: 1) { id name description status type } }|
    context = {}
    variables = {}
    @result = BudgetSchema.execute(query_string,
                                   context: context,
                                   variables: variables)
  end

  def test_account_show_query_returns_correct_account_id
    assert_equal '1', @result['data']['account']['id']
  end

  def test_account_show_query_returns_correct_account_name
    assert_equal 'Generic Account', @result['data']['account']['name']
  end

  def test_account_show_query_returns_correct_account_description
    assert_equal 'So generic', @result['data']['account']['description']
  end

  def test_account_show_query_returns_correct_account_status
    assert_equal 'active', @result['data']['account']['status']
  end

  def test_account_show_query_returns_correct_account_type
    assert_equal 'Generic', @result['data']['account']['type']
  end
end
