# frozen_string_literal: true

require 'test_helper'

class SignInUserHappyPathTest < ActiveSupport::TestCase
  def setup
    User.create!(email: 'test@test.co', password: 'secure')
    query_string = %|mutation {
    signInUser(authentication: {email: "test@test.co", password: "secure"}) {
    user {
      id email} authentication_token}}|
    context = {}
    variables = {}
    @result = BudgetSchema.execute(query_string,
                                   context: context,
                                   variables: variables)
  end

  def test_sign_in_user_query_returns_correct_user
    assert_equal 'test@test.co', @result['data']['signInUser']['user']['email']
  end

  def test_sign_in_user_query_returns_authentication_token
    refute_nil @result['data']['signInUser']['authentication_token']
  end
end
