# frozen_string_literal: true

require 'test_helper'

class CreateAccountSadPathTest < ActiveSupport::TestCase
  def setup
    @context = {}
    @variables = {}
  end

  def test_create_account_query_with_blank_name_returns_error
    query_string = %|mutation {
  createAccount(
input: {name: "", description: "Petty Cash", type: "Cash"}) {
  name description status type }
}|
    result = BudgetSchema.execute(query_string,
                                  context: @context,
                                  variables: @variables)
    message = "Invalid input for Account: Name can't be blank"
    assert_equal message, result['errors'][0]['message']
  end

  def test_create_account_query_with_blank_type_returns_error
    query_string = %|mutation {
  createAccount(
input: {name: "Wallet", description: "Petty Cash", type: ""}) {
  name description status type }
}|
    result = BudgetSchema.execute(query_string,
                                  context: @context,
                                  variables: @variables)
    message = 'Invalid input for Account: Type must be a valid Account type.'
    assert_equal message, result['errors'][0]['message']
  end

  def test_create_account_query_with_invalid_type_returns_error
    query_string = %|mutation {
  createAccount(
input: {name: "Wallet", description: "Petty Cash", type: "NotSavings"}) {
  name description status type }
}|
    result = BudgetSchema.execute(query_string,
                                  context: @context,
                                  variables: @variables)
    message = 'Invalid input for Account: Type must be a valid Account type.'
    assert_equal message, result['errors'][0]['message']
  end
end
