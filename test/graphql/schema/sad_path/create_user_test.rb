# frozen_string_literal: true

require 'test_helper'

class CreateUserSadPathTest < ActiveSupport::TestCase
  def setup
    @context = {}
    @variables = {}
  end

  def test_create_user_query_with_blank_email_returns_error
    query_string = %|mutation {
  createUser(authentication: {email: "", password: "password"}) {
    id
    email
  } }|
    @result = BudgetSchema.execute(query_string,
                                   context: @context,
                                   variables: @variables)
    message = "Invalid input for User: Email can't be blank"
    assert_match message, @result['errors'][0]['message']
  end

  def test_create_user_query_with_invalid_email_returns_error
    query_string = %|mutation {
  createUser(authentication: {email: "user@example", password: "password"}) {
    id
    email
  } }|
    @result = BudgetSchema.execute(query_string,
                                   context: @context,
                                   variables: @variables)
    message = 'Invalid input for User: Email is not a valid email address'
    assert_match message, @result['errors'][0]['message']
  end
end
