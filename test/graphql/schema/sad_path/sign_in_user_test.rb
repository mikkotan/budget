# frozen_string_literal: true

require 'test_helper'

class SignInUserSadPathTest < ActiveSupport::TestCase
  def setup
    User.create!(email: 'test@test.co', password: 'secure')
    @context = {}
    @variables = {}
  end

  def test_sign_in_user_with_blank_data
    query_string = %|mutation {
    signInUser(authentication: {email: "", password: ""}) {
    user {
      id email} authentication_token}}|
    result = BudgetSchema.execute(query_string,
                                  context: @context,
                                  variables: @variables)

    assert_nil result['data']['signInUser']
    assert_equal 'Email or Password is invalid', result['errors'][0]['message']
  end

  def test_sign_in_user_with_invalid_email
    query_string = %|mutation {
    signInUser(authentication: {email: "hello@test.co", password: "secure"}) {
    user {
      id email} authentication_token}}|
    result = BudgetSchema.execute(query_string,
                                  context: @context,
                                  variables: @variables)

    assert_nil result['data']['signInUser']
    assert_equal 'Email or Password is invalid', result['errors'][0]['message']
  end

  def test_sign_in_user_with_invalid_password
    query_string = %|mutation {
    signInUser(authentication: {email: "test@test.co", password: "secure123"}) {
    user {
      id email} authentication_token}}|
    result = BudgetSchema.execute(query_string,
                                  context: @context,
                                  variables: @variables)

    assert_nil result['data']['signInUser']
    assert_equal 'Email or Password is invalid', result['errors'][0]['message']
  end
end
