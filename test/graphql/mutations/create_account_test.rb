# frozen_string_literal: true

require 'test_helper'

class CreateAccount < ActiveSupport::TestCase
  def perform(args = {})
    Mutations::CreateAccount.new.call(nil, args, nil)
  end

  def test_create_account_success
    account = perform(
      input: {
        name: 'Petty Cash',
        description: 'Best account',
        type: 'Cash'
      }
    )

    assert account.persisted?
    assert_equal 'Petty Cash', account.name
    assert_equal 'Accounts::Cash', account.type
  end

  def test_create_account_blank_name_failure
    account = perform(
      input: {
        name: '',
        description: 'Best account',
        type: 'Cash'
      }
    )

    assert_match "Name can't be blank", account.message
  end

  def test_create_account_blank_description_success
    account = perform(
      input: {
        name: 'Petty Cash',
        description: '',
        type: 'Cash'
      }
    )

    assert account.persisted?
    assert_equal '', account.description
  end

  def test_create_account_blank_type_failure
    account = perform(
      input: {
        name: 'Petty Cash',
        description: 'Best account',
        type: ''
      }
    )

    assert_match 'Type must be a valid Account type.', account.message
  end

  def test_create_account_invalid_type_failure
    account = perform(
      input: {
        name: 'Petty Cash',
        description: 'Best account',
        type: 'Random'
      }
    )

    assert_match 'Type must be a valid Account type.', account.message
  end
end
