# frozen_string_literal: true

require 'test_helper'

class SignInUserTest < ActiveSupport::TestCase
  def perform(args = {})
    Mutations::SignInUser.new.call(nil, args, cookies: {})
  end

  def setup
    @user = User.create!(email: 'hello@example.com', password: 'secure')
  end

  def test_sign_in_success
    result = perform(
      authentication: {
        email: 'hello@example.com',
        password: 'secure'
      }
    )

    assert result.present?
  end

  def test_sign_in_with_blank_data
    result = perform

    assert_equal 'Email or Password is invalid', result.message
  end

  def test_sign_in_with_invalid_email
    result = perform(
      authentication: {
        email: 'hello@example',
        password: 'secure'
      }
    )

    assert_equal 'Email or Password is invalid', result.message
  end

  def test_sign_in_with_invalid_password
    result = perform(
      authentication: {
        email: 'hello@example.com',
        password: 'secure123'
      }
    )

    assert_equal 'Email or Password is invalid', result.message
  end
end
