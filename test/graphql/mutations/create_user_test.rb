# frozen_string_literal: true

require 'test_helper'

class CreateUserTest < ActiveSupport::TestCase
  def perform(args = {})
    Mutations::CreateUser.new.call(nil, args, nil)
  end

  def test_create_user_success
    user = perform(
      authentication: {
        email: 'email@example.com',
        password: 'supersecurepassword123'
      }
    )

    assert user.persisted?
    assert_equal 'email@example.com', user.email
  end

  def test_create_user_invalid_email
    user = perform(
      authentication: {
        email: 'email@example',
        password: 'supersecurepassword123'
      }
    )

    assert_match 'Email is not a valid email address.', user.message
  end
end
