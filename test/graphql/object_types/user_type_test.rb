# frozen_string_literal: true

require 'test_helper'

class UserTypeTest < ActiveSupport::TestCase
  def setup
    @user_type = BudgetSchema.types['User']
  end

  def test_query_object_returns_id
    assert @user_type.fields.keys.include?('id')
  end

  def test_query_object_returns_email
    assert @user_type.fields.keys.include?('email')
  end
end
