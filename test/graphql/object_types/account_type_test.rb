# frozen_string_literal: true

require 'test_helper'

class AccountTypeTest < ActiveSupport::TestCase
  def setup
    @account_type = BudgetSchema.types['Account']
  end

  def test_query_object_returns_id
    assert @account_type.fields.keys.include?('id')
  end

  def test_query_object_returns_name
    assert @account_type.fields.keys.include?('name')
  end

  def test_query_object_returns_description
    assert @account_type.fields.keys.include?('description')
  end

  def test_query_object_returns_status
    assert @account_type.fields.keys.include?('status')
  end

  def test_query_object_returns_type
    assert @account_type.fields.keys.include?('type')
  end
end
