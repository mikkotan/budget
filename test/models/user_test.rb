# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = users(:admin)
  end

  def test_user_is_valid
    assert @user.valid?
  end

  def test_user_with_blank_email
    @user.email = ''
    refute @user.valid?
  end

  def test_user_with_invalid_email
    @user.email = 'test@example'
    refute @user.valid?
  end
end
