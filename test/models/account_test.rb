# frozen_string_literal: true

require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  def setup
    @account = accounts(:generic_account)
  end

  def test_account_is_valid
    assert @account.valid?
  end

  def test_account_without_name
    @account.name = ''
    refute @account.valid?
    assert_not_nil @account.errors[:name]
  end

  def test_account_without_description
    @account.description = ''
    assert @account.valid?
  end

  def test_account_without_type
    @account.type = ''
    refute @account.valid?
    assert_not_nil @account.errors[:type]
  end

  def test_account_with_invalid_type
    @account.type = 'Random'
    refute @account.valid?
    assert_not_nil @account.errors[:type]
  end
end
