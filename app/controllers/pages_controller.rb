# frozen_string_literal: true

# Controller class for the site's static pages
class PagesController < ApplicationController
  def home; end
end
