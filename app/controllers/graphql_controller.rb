# frozen_string_literal: true

# Controller for GraphQL
class GraphqlController < ActionController::Base
  def execute
    variables = ensure_hash(params[:variables])
    query = params[:query]
    operation_name = params[:operationName]
    context = { current_user: current_user }
    result = BudgetSchema.execute(query,
                                  variables: variables,
                                  context: context,
                                  operation_name: operation_name)
    render json: result
  end

  private

  # Handle form data, JSON body, or a blank value
  def ensure_hash(ambiguous_param)
    case ambiguous_param
    when String
      transform_param_to_json(ambiguous_param)
    when Hash, ActionController::Parameters
      ambiguous_param
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{ambiguous_param}"
    end
  end

  def transform_param_to_json(ambiguous_param)
    if ambiguous_param.present?
      ensure_hash(JSON.parse(ambiguous_param))
    else
      {}
    end
  end

  def current_user
    return unless authorization_token && authorized_email

    User.find_by(email: authorized_email,
                 authentication_token: authorization_token)
  end

  def authorization_token
    pattern = /^Bearer /
    header = request.headers['Authorization']
    header.gsub(pattern, '') if header && header.match(pattern)
  end

  def authorized_email
    request.headers['AuthorizedEmail']
  end
end
