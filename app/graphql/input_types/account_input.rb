# frozen_string_literal: true

module InputTypes
  AccountInput = GraphQL::InputObjectType.define do
    name 'AccountInput'
    description 'Input fields for creating/updating an Account'

    argument :name, !types.String do
      description 'Name of the Account.'
    end

    argument :description, types.String do
      description 'Description of the Account.'
    end

    argument :type, !types.String do
      description 'Type of Account.'
    end
  end
end
