# frozen_string_literal: true

module InputTypes
  AuthenticationInput = GraphQL::InputObjectType.define do
    name 'AuthenticationCredentialsInput'
    description 'Input fields for authenticating inside the application'

    argument :email, !types.String do
      description 'Email address of the user'
    end

    argument :password, !types.String do
      description "Password of the user's account"
    end
  end
end
