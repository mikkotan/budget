# frozen_string_literal: true

QueryType = GraphQL::ObjectType.define do
  name 'Query'
  description 'The query root for this schema'

  field :accounts, function: Queries::ListAccounts.new
  field :account, function: Queries::ShowAccount.new
end
