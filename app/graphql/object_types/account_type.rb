# frozen_string_literal: true

module ObjectTypes
  AccountType = GraphQL::ObjectType.define do
    name 'Account'
    description "Account is the representation of a User's financial account"
    field :id, !types.ID
    field :name, !types.String
    field :description, !types.String
    field :status, !types.String
    field :type, !types.String do
      resolve ->(obj, _args, _ctx) {
        obj.type_name
      }
    end
  end
end
