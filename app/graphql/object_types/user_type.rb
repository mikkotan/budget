# frozen_string_literal: true

module ObjectTypes
  UserType = GraphQL::ObjectType.define do
    name 'User'

    field :id, !types.ID
    field :email, !types.String
  end
end
