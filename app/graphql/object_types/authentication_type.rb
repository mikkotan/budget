# frozen_string_literal: true

module ObjectTypes
  AuthenticationType = GraphQL::ObjectType.define do
    name 'Authentication'

    field :user, !ObjectTypes::UserType
    field :authentication_token, !types.String
  end
end
