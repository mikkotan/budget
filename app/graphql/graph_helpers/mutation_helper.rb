# frozen_string_literal: true

module GraphHelpers
  # Utility methods for mutation classes
  module MutationHelper
    def return_error_response(obj)
      message = ''
      if obj.is_a? String
        message = obj
      else
        error_messages = obj.errors.full_messages.join(',')
        message = "Invalid input for #{obj.class}: #{error_messages}"
      end
      GraphQL::ExecutionError.new(message)
    end
  end
end
