# frozen_string_literal: true

MutationType = GraphQL::ObjectType.define do
  name 'Mutation'

  field :createAccount, function: Mutations::CreateAccount.new
  field :createUser, function: Mutations::CreateUser.new
  field :signInUser, function: Mutations::SignInUser.new
end
