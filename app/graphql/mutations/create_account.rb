# frozen_string_literal: true

module Mutations
  # CreateAccount GraphQL endpoint
  # You can test this endpoint by sending
  # a request to POST /graphql with this:
  #
  # mutation {
  #   createAccount(input: {name: 'Cash', description: '', type: 'Cash'}) {
  #     id
  #     name
  #     description
  #     type
  #     status
  #   }
  # }
  class CreateAccount < GraphQL::Function
    include GraphHelpers::MutationHelper

    description 'Creates a new Account'
    argument :input, InputTypes::AccountInput
    type ObjectTypes::AccountType

    def call(_obj, args, _ctx)
      account = Account.new
      if account.update(args[:input].to_h)
        account
      else
        return_error_response(account)
      end
    end
  end
end
