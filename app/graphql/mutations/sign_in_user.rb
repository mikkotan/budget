# frozen_string_literal: true

module Mutations
  # SignInUser GraphQL endpoint
  # You can test this endpoint by sending
  # a request to POST /graphql with this:
  #
  # mutation {
  #   signInUser(authentication: {email: 'hi@hello.com', password: 'secure'}) {
  #     user {
  #       id
  #       email
  #     }
  #     authentication_token
  #   }
  # }
  class SignInUser < GraphQL::Function
    include GraphHelpers::MutationHelper

    description 'Authenticates a User into the app'
    argument :authentication, !InputTypes::AuthenticationInput
    type ObjectTypes::AuthenticationType

    def call(_obj, args, _ctx)
      email = args[:authentication].try(:[], :email)
      password = args[:authentication].try(:[], :password)

      user = User.find_by(email: email)
      msg = 'Email or Password is invalid'
      return return_error_response(msg) unless authenticated?(user, password)

      user.regenerate_authentication_token
      OpenStruct.new(user: user,
                     authentication_token: user.authentication_token)
    end

    def authenticated?(user, password)
      user && user.try(:valid_password?, password)
    end
  end
end
