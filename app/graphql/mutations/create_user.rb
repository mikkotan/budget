# frozen_string_literal: true

module Mutations
  # CreateUser GraphQL endpoint
  # You can test this endpoint by sending
  # a request to POST /graphql with this:
  #
  # mutation {
  #   createUser(authentication: {email: 'hi@hello.com', password: 'secure'}) {
  #     id
  #     email
  #   }
  # }
  class CreateUser < GraphQL::Function
    include GraphHelpers::MutationHelper

    description 'Creates a new User'
    argument :authentication, !InputTypes::AuthenticationInput
    type ObjectTypes::UserType

    def call(_obj, args, _ctx)
      user = User.new
      if user.update(email: args[:authentication][:email],
                     password: args[:authentication][:password])
        user
      else
        return_error_response(user)
      end
    end
  end
end
