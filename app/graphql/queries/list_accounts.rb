# frozen_string_literal: true

module Queries
  # ListAccounts GraphQL endpoint
  # You can test this endpoint by sending
  # a request to POST /graphql with this:
  #
  # query {
  #   accounts {
  #     id
  #     name
  #     description
  #     type
  #     status
  #   }
  # }
  class ListAccounts < GraphQL::Function
    description 'Get all Accounts'
    type ObjectTypes::AccountType.to_list_type

    def call(_obj, _args, _ctx)
      Account.all
    end
  end
end
