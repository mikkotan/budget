# frozen_string_literal: true

module Queries
  # ShowAccounts GraphQL endpoint
  # You can test this endpoint by sending
  # a request to POST /graphql with this:
  #
  # query {
  #   account(id: 1) {
  #     id
  #     name
  #     description
  #     type
  #     status
  #   }
  # }
  class ShowAccount < GraphQL::Function
    description 'Find an Account by ID'
    argument :id, !types.ID
    type ObjectTypes::AccountType

    def call(_obj, args, _ctx)
      Account.find(args.to_h.with_indifferent_access['id'])
    end
  end
end
