# frozen_string_literal: true

BudgetSchema = GraphQL::Schema.define do
  mutation(MutationType)
  query(QueryType)
end
