import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import Drawer from 'material-ui/Drawer';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import MenuIcon from 'material-ui-icons/Menu';
import HomeIcon from 'material-ui-icons/Home';
import AccountBalanceIcon from 'material-ui-icons/AccountBalance';

import { requestSignOut, receiveSignOut } from '../../actions/sign_out';
import { openDrawer, closeDrawer, toggleDrawer } from '../../actions/header';

const SignInButton = () => (
  <Button
    color="contrast"
    component={Link}
    to="/users/sign_in"
  >
    Sign In
  </Button>
);

const SignOutButton = ({ onSignOutClick }) => (
  <Button
    color="contrast"
    component={Link}
    to="/"
    onClick={onSignOutClick}
  >
    Sign Out
  </Button>
);

SignOutButton.propTypes = {
  onSignOutClick: PropTypes.func.isRequired,
};

const HeaderMenu = ({ open, onRequestClose, onMenuItemClick }) => (
  <Drawer
    open={open}
    onRequestClose={onRequestClose}
  >
    <List>
      <ListItem
        button
        component={Link}
        to="/"
        onClick={onMenuItemClick}
      >
        <ListItemIcon>
          <HomeIcon />
        </ListItemIcon>
        <ListItemText primary="Home" />
      </ListItem>
      <ListItem
        button
        component={Link}
        to="/accounts"
        onClick={onMenuItemClick}
      >
        <ListItemIcon>
          <AccountBalanceIcon />
        </ListItemIcon>
        <ListItemText primary="Accounts" />
      </ListItem>
    </List>
  </Drawer>
);

HeaderMenu.propTypes = {
  open: PropTypes.bool.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  onMenuItemClick: PropTypes.func.isRequired,
};

const Header = ({
  onSignOutClick,
  onRequestClose,
  onMenuBarClick,
  onMenuItemClick,
  isAuthenticated,
  open,
}) => (
  <div>
    <AppBar position="static">
      <Toolbar>
        <IconButton
          color="contrast"
          aria-label="Menu"
          onClick={onMenuBarClick}
        >
          <MenuIcon />
        </IconButton>
        <Typography type="title" color="inherit">
          Budget
        </Typography>
        {isAuthenticated ?
          <SignOutButton onSignOutClick={onSignOutClick} />
          :
          <SignInButton />
        }
      </Toolbar>
    </AppBar>

    <HeaderMenu
      open={open}
      onRequestClose={onRequestClose}
      onMenuItemClick={onMenuItemClick}
    />
  </div>
);

Header.propTypes = {
  onSignOutClick: PropTypes.func.isRequired,
  onRequestClose: PropTypes.func.isRequired,
  onMenuBarClick: PropTypes.func.isRequired,
  onMenuItemClick: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  open: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  isAuthenticated: state.authentication.isAuthenticated,
  open: state.header.open,
});

const mapDispatchToProps = dispatch => ({
  onSignOutClick: () => {
    dispatch(requestSignOut());
    dispatch(receiveSignOut());
  },
  onRequestClose: () => {
    dispatch(closeDrawer());
  },
  onMenuBarClick: () => {
    dispatch(openDrawer());
  },
  onMenuItemClick: () => {
    dispatch(closeDrawer());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
