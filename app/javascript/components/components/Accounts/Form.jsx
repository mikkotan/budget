import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { MenuItem } from 'material-ui/Menu';
import Button from 'material-ui/Button';
import { FormControl } from 'material-ui/Form';
import { InputLabel } from 'material-ui/Input';

import { renderTextField, renderSelectField } from '../MaterialUIForm';

const AccountForm = ({ onSubmit, handleSubmit }) => (
  <form onSubmit={handleSubmit(onSubmit)}>
    <div>
      <FormControl>
        <Field
          name="name"
          component={renderTextField}
          placeholder="Petty Cash, BDO Savings"
          label="Account Name"
          type="text"
        />
      </FormControl>
    </div>
    <div>
      <FormControl>
        <Field
          name="description"
          component={renderTextField}
          placeholder="Short description about this account"
          label="Description"
          type="text"
          multiLine
        />
      </FormControl>
    </div>
    <div>
      <FormControl>
        <InputLabel htmlFor="type">Type</InputLabel>
        <Field
          id="type"
          name="type"
          component={renderSelectField}
        >
          <MenuItem value="Cash">Cash</MenuItem>
          <MenuItem value="Savings">Savings</MenuItem>
          <MenuItem value="Checking">Checking</MenuItem>
          <MenuItem value="CreditCard">Credit Card</MenuItem>
        </Field>
      </FormControl>
    </div>
    <Button
      color="primary"
      raised
      type="submit"
    >
      Submit
    </Button>
  </form>
);

AccountForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({ form: 'AccountForm' })(AccountForm);
