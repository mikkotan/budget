import React from 'react';
import PropTypes from 'prop-types';
import { gql, graphql } from 'react-apollo';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import {
  createAccountRequest,
  createAccountSuccess,
  createAccountFailure,
  createAccountReset,
} from '../../../../actions/accounts';
import accountsListQuery from '../List/queries';
import AccountForm from '../../Form';

const AccountCreate = ({ success, onSubmit }) => {
  if (success) {
    return <Redirect to="/accounts" />;
  }

  return (
    <AccountForm onSubmit={onSubmit} />
  );
};

AccountCreate.propTypes = {
  success: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

const createAccountMutation = gql`
  mutation createAccount($input: AccountInput!) {
    createAccount(input: $input) {
      id
      name
      description
      status
      type
    }
  }
`;

const AccountCreateWithData = graphql(createAccountMutation, {
  props: ({ mutate }) => ({
    createAccount: account => mutate({
      variables: {
        input: {
          name: account.name,
          description: account.description,
          type: account.type,
        },
      },
      update: (store, { data: { createAccount } }) => {
        const data = store.readQuery({ query: accountsListQuery });
        data.accounts.push(createAccount);
        store.writeQuery({ query: accountsListQuery, data });
      },
    }),
  }),
});

const mapStateToProps = state => ({
  success: state.accounts.createAccount.success,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onSubmit: ({ name, description, type }) => {
    dispatch(createAccountRequest());

    ownProps.createAccount({ name, description, type })
      .then((result) => {
        dispatch(createAccountSuccess(result.data.createAccount));
        dispatch(createAccountReset());
      }).catch((error) => {
        dispatch(createAccountFailure(error));
      });
  },
});

const connectedForm = connect(mapStateToProps, mapDispatchToProps)(AccountCreate);

export default withRouter(AccountCreateWithData(connectedForm));
