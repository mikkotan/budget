import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-apollo';
import { Link } from 'react-router-dom';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';

import accountsListQuery from './queries';

const AddAccountButton = () => (
  <Button
    color="primary"
    component={Link}
    to="/accounts/new"
    raised
  >
    Add Account
  </Button>
);

const AccountList = ({ data: { loading, error, accounts } }) => {
  if (loading) {
    return <p>Loading...</p>;
  }

  if (error) {
    return <p>{error.message}</p>;
  }

  return (
    <div>
      <AddAccountButton />
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>Type</TableCell>
              <TableCell>Status</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            { accounts.map(account =>
              (<TableRow key={account.id}>
                <TableCell>{account.id}</TableCell>
                <TableCell>{account.name}</TableCell>
                <TableCell>{account.description}</TableCell>
                <TableCell>{account.type}</TableCell>
                <TableCell>{account.status}</TableCell>
              </TableRow>),
            ) }
          </TableBody>
        </Table>
      </Paper>
      <AddAccountButton />
    </div>
  );
};

AccountList.propTypes = {
  data: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    error: PropTypes.string,
    accounts: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        description: PropTypes.string,
        type: PropTypes.string.isRequired,
        status: PropTypes.string.isRequired,
      }).isRequired,
    ),
  }).isRequired,
};

const AccountListWithData = graphql(accountsListQuery)(AccountList);

export default AccountListWithData;
