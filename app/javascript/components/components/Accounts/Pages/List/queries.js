import { gql } from 'react-apollo';

const accountsListQuery = gql`
  query {
    accounts {
      id
      name
      description
      type
      status
    }
  }
`;

export default accountsListQuery;
