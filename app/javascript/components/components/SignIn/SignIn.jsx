import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, withRouter } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import { gql, graphql } from 'react-apollo';
import { connect } from 'react-redux';
import Button from 'material-ui/Button';
import { FormControl } from 'material-ui/Form';

import {
  requestSignIn,
  receiveSignIn,
  signInError,
} from '../../actions/sign_in';
import { renderTextField } from '../MaterialUIForm';

const SignInForm = ({ onSubmit, isAuthenticated, handleSubmit }) => {
  if (isAuthenticated) {
    return <Redirect to="/" />;
  }

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
    >
      <div>
        <FormControl>
          <Field
            name="email"
            component={renderTextField}
            placeholder="test@example.com"
            label="Email"
            type="email"
          />
        </FormControl>
      </div>
      <div>
        <FormControl>
          <Field
            name="password"
            component={renderTextField}
            placeholder="Password"
            label="Password"
            type="password"
          />
        </FormControl>
      </div>
      <Button
        color="primary"
        raised
        type="submit"
      >
        Sign In
      </Button>
    </form>
  );
};

SignInForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};

const signInUserMutation = gql`
  mutation signInUser($authentication: AuthenticationCredentialsInput!) {
    signInUser(authentication: $authentication) {
      user {
        id
        email
      }
      authentication_token
    }
  }
`;

const SignInWithData = graphql(signInUserMutation, {
  props: ({ mutate }) => ({
    signInUser: authentication => mutate({
      variables: { authentication },
    }),
  }),
});

const mapStateToProps = state => ({
  isAuthenticated: state.authentication.isAuthenticated,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onSubmit: ({ email, password }) => {
    dispatch(requestSignIn({ email }));

    ownProps.signInUser({ email, password })
      .then((result) => {
        dispatch(receiveSignIn(result.data.signInUser));
      }).catch((error) => {
        dispatch(signInError({ error }));
      });
  },
});

const SmartForm = reduxForm({ form: 'SignInForm' })(SignInForm);

export default withRouter(SignInWithData(connect(mapStateToProps, mapDispatchToProps)(SmartForm)));
