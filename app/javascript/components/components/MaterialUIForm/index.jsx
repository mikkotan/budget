import React from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import Select from 'material-ui/Select';

export const renderSelectField = ({ input, children }) => (
  <Select
    {...input}
    onChange={event => input.onChange(event.target.value)}
  >
    {children}
  </Select>
);

renderSelectField.propTypes = {
  input: PropTypes.shape({
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onDragStart: PropTypes.func.isRequired,
    onDrop: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
  }).isRequired,
  children: PropTypes.arrayOf(
    PropTypes.instanceOf(Object).isRequired,
  ).isRequired,
};

export const renderTextField = ({
  placeholder,
  label,
  type,
  input,
  multiline,
}) => (
  <TextField
    placeholder={placeholder}
    label={label}
    type={type}
    {...input}
    multiline={multiline}
  />
);

renderTextField.propTypes = {
  placeholder: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  input: PropTypes.shape({
    onBlur: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    onDragStart: PropTypes.func.isRequired,
    onDrop: PropTypes.func.isRequired,
    onFocus: PropTypes.func.isRequired,
  }).isRequired,
  multiline: PropTypes.bool,
};

renderTextField.defaultProps = {
  multiline: false,
};
