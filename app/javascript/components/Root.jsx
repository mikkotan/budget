import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import history from './history';

import Home from './components/Home';
import AccountList from './components/Accounts/Pages/List';
import AccountCreate from './components/Accounts/Pages/Create';
import Header from './components/Header';
import SignIn from './components/SignIn';

const theme = createMuiTheme({
  palette: {
    type: 'light',
  },
});

const Root = () => (
  <ConnectedRouter history={history}>
    <Router>
      <MuiThemeProvider theme={theme}>
        <div>
          <Header />
          <div>
            <Grid container spacing={24}>
              <Route exact path="/" component={Home} />
              <Route exact path="/accounts" component={AccountList} />
              <Route path="/accounts/new" component={AccountCreate} />
              <Route path="/users/sign_in" component={SignIn} />
            </Grid>
          </div>
        </div>
      </MuiThemeProvider>
    </Router>
  </ConnectedRouter>
);

export default Root;
