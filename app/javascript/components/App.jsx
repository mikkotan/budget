import React from 'react';
import { ApolloProvider } from 'react-apollo';

import store from './store';
import apolloClient from './apollo_client';
import Root from './Root';

const App = () => (
  <ApolloProvider store={store} client={apolloClient}>
    <Root />
  </ApolloProvider>
);

export default App;
