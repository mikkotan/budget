import {
  DRAWER_OPEN,
  DRAWER_CLOSE,
} from '../actions/header';

const initialState = {
  open: false,
};

const header = (state = initialState, { type }) => {
  switch (type) {
    case DRAWER_OPEN:
      return {
        open: true,
      };
    case DRAWER_CLOSE:
      return {
        open: false,
      };
    default:
      return state;
  }
};

export default header;
