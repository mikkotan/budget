import {
  CREATE_ACCOUNT_REQUEST,
  CREATE_ACCOUNT_SUCCESS,
  CREATE_ACCOUNT_FAILURE,
  CREATE_ACCOUNT_RESET,
} from '../actions/accounts';

const initialState = {
  createAccount: {
    isFetching: false,
    message: null,
    success: false,
    account: null,
  },
};

const accounts = (state = initialState, { type, ...payload }) => {
  switch (type) {
    case CREATE_ACCOUNT_REQUEST:
      return {
        ...state,
        createAccount: {
          isFetching: true,
          message: null,
          success: false,
          account: null,
        },
      };
    case CREATE_ACCOUNT_SUCCESS:
      return {
        ...state,
        createAccount: {
          isFetching: false,
          message: null,
          success: true,
          account: payload.account,
        },
      };
    case CREATE_ACCOUNT_FAILURE:
      return {
        ...state,
        createAccount: {
          isFetching: false,
          message: payload.errorMessage,
          success: false,
          account: null,
        },
      };
    case CREATE_ACCOUNT_RESET:
      return {
        ...state,
        createAccount: {
          isFetching: false,
          message: null,
          success: false,
          account: null,
        },
      };
    default:
      return state;
  }
};

export default accounts;
