import {
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE,
} from '../actions/sign_in';
import {
  SIGN_OUT_REQUEST,
  SIGN_OUT_SUCCESS,
} from '../actions/sign_out';

const initialState = {
  isAuthenticated: !!localStorage.getItem('authentication_token'),
  authenticationToken: localStorage.getItem('authentication_token'),
  currentUserEmail: localStorage.getItem('current_user_email'),
  loading: false,
  errorMessage: null,
  successMessage: null,
};

const authentication = (state = initialState, { type, ...payload }) => {
  switch (type) {
    case SIGN_IN_REQUEST:
      return {
        ...initialState,
        loading: true,
        currentUserEmail: payload.currentUserEmail,
      };
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        authenticationToken: payload.authenticationToken,
        successMessage: payload.message,
        loading: false,
      };
    case SIGN_IN_FAILURE:
      return {
        ...state,
        errorMessage: payload.message,
        loading: false,
      };
    case SIGN_OUT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case SIGN_OUT_SUCCESS:
      return {
        ...state,
        loading: false,
        isAuthenticated: false,
        authenticationToken: null,
        currentUserEmail: null,
        successMessage: payload.message,
      };
    default:
      return state;
  }
};

export default authentication;
