import {
  combineReducers,
  createStore,
  compose,
  applyMiddleware,
} from 'redux';
import { createLogger } from 'redux-logger';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { reducer as formReducer } from 'redux-form';

import apolloClient from './apollo_client';
import history from './history';
import signInReducer from './reducers/authentication';
import headerReducer from './reducers/header';
import accountsReducer from './reducers/accounts';

const reducers = combineReducers({
  apollo: apolloClient.reducer(),
  authentication: signInReducer,
  form: formReducer,
  header: headerReducer,
  accounts: accountsReducer,
});

const store = createStore(
  connectRouter(history)(reducers),
  {},
  compose(
    applyMiddleware(
      apolloClient.middleware(),
      createLogger(),
      routerMiddleware(history),
    ),
    window.devToolsExtension ? window.devToolsExtension() : f => f,
  ),
);

export default store;
