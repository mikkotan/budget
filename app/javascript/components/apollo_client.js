import { ApolloClient, createNetworkInterface } from 'react-apollo';

const networkInterface = createNetworkInterface({
  uri: '/graphql',
});

networkInterface.use([{
  applyMiddleware(req, next) {
    if (!req.options.headers) {
      req.options.headers = {};
    }
    const authToken = localStorage.getItem('authentication_token');
    const authEmail = localStorage.getItem('current_user_email');
    req.options.headers.Authorization = authToken ? `Bearer ${authToken}` : null;
    req.options.headers.AuthorizedEmail = authEmail || null;
    next();
  },
}]);

export default new ApolloClient({ networkInterface });
