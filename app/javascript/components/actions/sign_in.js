export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE';

export const requestSignIn = currentUserEmail => ({
  type: SIGN_IN_REQUEST,
  isFetching: true,
  isAuthenticated: false,
  message: null,
  currentUserEmail,
});

export const receiveSignIn = (auth) => {
  localStorage.setItem('current_user_email', auth.user.email);
  localStorage.setItem('authentication_token', auth.authentication_token);

  return {
    type: SIGN_IN_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    currentUserEmail: auth.user.email,
    authenticationToken: auth.authentication_token,
    message: 'You have successfully signed in!',
  };
};

export const signInError = ({ error }) => ({
  type: SIGN_IN_FAILURE,
  isFetching: false,
  isAuthenticated: false,
  message: error.message,
});
