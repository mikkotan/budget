export const SIGN_OUT_REQUEST = 'SIGN_OUT_REQUEST';
export const SIGN_OUT_SUCCESS = 'SIGN_OUT_SUCCESS';

export const requestSignOut = () => ({
  type: SIGN_OUT_REQUEST,
  isFetching: true,
  isAuthenticated: true,
  currentUserEmail: localStorage.getItem('current_user_email'),
  authenticationToken: localStorage.getItem('authentication_token'),
});

export const receiveSignOut = () => {
  localStorage.removeItem('current_user_email');
  localStorage.removeItem('authentication_token');

  return {
    type: SIGN_OUT_SUCCESS,
    isFetching: false,
    isAuthenticated: false,
    currentUserEmail: null,
    authenticationToken: null,
  };
};
