export const CREATE_ACCOUNT_REQUEST = 'CREATE_ACCOUNT_REQUEST';
export const CREATE_ACCOUNT_SUCCESS = 'CREATE_ACCOUNT_SUCCESS';
export const CREATE_ACCOUNT_FAILURE = 'CREATE_ACCOUNT_FAILURE';
export const CREATE_ACCOUNT_RESET = 'CREATE_ACCOUNT_RESET';

export const createAccountRequest = () => ({
  type: CREATE_ACCOUNT_REQUEST,
});

export const createAccountSuccess = account => ({
  type: CREATE_ACCOUNT_SUCCESS,
  account,
});

export const createAccountFailure = error => ({
  type: CREATE_ACCOUNT_FAILURE,
  errorMessage: error.message,
});

export const createAccountReset = () => ({
  type: CREATE_ACCOUNT_RESET,
});
