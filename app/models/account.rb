# frozen_string_literal: true

# Account is the representation of a User's real-life accounts
# We support the following types of accounts:
# - Cash (CashAccount)
# - Checking (CheckingAccount)
# - Savings (SavingsAccount)
# - Credit Card (CreditCardAccount)
class Account < ApplicationRecord
  enum status: %i[active closed]

  validates :name, presence: true
  validate :ensure_type_is_fulfilled

  def type_name
    type.to_s.gsub('Accounts::', '')
  end

  private

  def ensure_type_is_fulfilled
    return true if valid_account_types.include?(type)

    type_passed = type.titleize.sub(' ', '')

    if %w[Cash Checking CreditCard Savings].include?(type_passed)
      self.type = "Accounts::#{type_passed}"
    else
      errors.add(:type, 'must be a valid Account type.')
    end
  end

  def valid_account_types
    [
      'Accounts::Cash',
      'Accounts::Checking',
      'Accounts::CreditCard',
      'Accounts::Generic',
      'Accounts::Savings'
    ]
  end
end
