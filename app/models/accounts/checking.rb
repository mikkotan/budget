# frozen_string_literal: true

# Checking accounts are a kind of deposit account that
# are more liquid compared to Savings accounts.
module Accounts
  class Checking < Account
  end
end
