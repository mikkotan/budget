# frozen_string_literal: true

# Credit cards are payment cards that can be used to
# buy goods and services without having to use cash or cheque
module Accounts
  class CreditCard < Account
  end
end
