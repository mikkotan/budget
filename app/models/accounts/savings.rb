# frozen_string_literal: true

# Savings accounts are the most basic kind of deposit account
# that you can open. Most banks offer savings accounts
module Accounts
  class Savings < Account
  end
end
