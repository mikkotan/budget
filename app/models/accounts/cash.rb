# frozen_string_literal: true

# Cash is one of the types of Account a User can create
# It's commonly used to represent cash-based accounts like:
# - Petty Cash
# - Wallet
# - Secret Stash
module Accounts
  class Cash < Account
  end
end
