# frozen_string_literal: true

# Generic Accounts are simply just a placeholder
# We should not have any records that are classified
# as Generic ever.
module Accounts
  class Generic < Account
  end
end
