# frozen_string_literal: true

# The User model. Runs on devise
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_secure_token :authentication_token

  validates :email, email_format: { message: 'is not a valid email address.' }

  before_save :update_auth_token_time, if: :authentication_token_changed?

  private

  def update_auth_token_time
    self.authentication_token_generated_at = Time.now
  end
end
