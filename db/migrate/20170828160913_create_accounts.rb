class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.string :name
      t.text :description
      t.integer :status, default: 0
      t.string :type

      t.timestamps
    end
  end
end
