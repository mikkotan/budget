class AddAuthenticationTokenGeneratedAtToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :authentication_token_generated_at, :datetime
  end
end
